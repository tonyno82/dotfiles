;; NVIM CONFIG BY ATANASIO "GROCTEL" RUBIO ---- git@taxorubio.com

(set _G.__luacache_config {:modpaths {:enable false}})
(let [(ok? impatient) (pcall require :impatient)]
  (when ok? (impatient.enable_profile)))

(when vim.opt.compatible (set vim.opt.compatible false))

(set _G.GROCTEL_WORK_PC false)
(set _G.GROCTEL_PLUGDEV true)

(require :plugins)
(require :user)
