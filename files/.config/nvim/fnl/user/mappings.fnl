(local keymap_set vim.keymap.set)
(local keymap_opts {:noremap true})


(macro map! [mode keys action]
  `(keymap_set ,mode ,keys ,action keymap_opts))

(macro nmap! [keys action]
  `(map! :n ,keys ,action))

(macro tmap! [keys action]
  `(map! :t ,keys ,action))

(macro xmap! [keys action]
  `(map! :x ,keys ,action))


(nmap! :n :nzz) ;; Center search results
(nmap! :N :Nzz) ;; ''
(nmap! :Q "") ;; Disable Ex mode
(nmap! :<BS> "<C-^>`\"zz") ;; Return to the last active buffer centering the line you were in

(tmap! :<ESC><ESC> :<C-\><C-n>) ;; Exit from terminal mode

(xmap! :. :<cmd>normal!.<cr>) ;; Repeat the last action on visual selections
(xmap! :< :<gv) ;; Stay in visual mode after indenting
(xmap! :> :>gv) ;; ''
