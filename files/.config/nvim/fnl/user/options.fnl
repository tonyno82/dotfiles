(local opt vim.opt)

(macro set! [options]
  (icollect [option value (pairs options) &into `(do)]
    `(tset opt ,option ,value)))


(fn _G.fold_string []
  (let [foldstart vim.v.foldstart
        line_count (+ (- vim.v.foldend foldstart) 1)
        first_line (vim.fn.getline foldstart)]
    (.. "   " first_line " (" line_count " lines)")))


(set! {:autoindent true ;; Start new lines correctly indented
       :colorcolumn [81] ;; Draw column at line character limit
       :completeopt [:menuone :noselect] ;; Completion engine options
       :cursorline true ;; Highlight the line where the cursor is (see cursorlineopt)
       :cursorlineopt :number ;; Highlight the cursor line number (see cursorline)
       :expandtab true ;; Expand tabs to spaces (see softtabstop)
       :fillchars {:diff "╱" :fold " "} ;; Interface styling (see listchars)
       :fixeol true ;; Restore EOL at EOF if missing when writing
       :foldmethod :marker ;; Only allow foldings with triple brackets
       :guicursor {:a :block} ;; Force cursor to be a block at all times
       :foldtext "v:lua.fold_string()" ;; Custom folding string
       :hidden true ;; Hide inactive buffers instead of deleting them
       :hlsearch true ;; Highlight all search matches
       :inccommand :split ;; Incrementally show effects of commands, opens split
       :incsearch true ;; Highlight search matches while writing (with hlsearch)
       :laststatus 3 ;; Use a global statusline instead of one per window
       :linebreak true ;; Respect WORDS when wrap-breaking lines (see wrap)
       :list true ;; Replace special characters (see listchars)
       :listchars {:tab "🭱 " :trail "·" } ;; Alternate tab: »> (see list)
       :mouse :nvi ;; Allow mouse everywhere except in command line mode
       :nrformats :unsigned ;; Treat all numbers as unsigned with <C-A> and <C-X>
       :number true ;; Number column to the left (used with relativenumber)
       :relativenumber true ;; Show numbers relative to cursor position (see number)
       :scrolloff 5 ;; Leave 5 lines above and below cursor
       :shiftwidth 0 ;; Force indent spaces to equal to tabstop (see tabstop)
       :showcmd true ;; Show the keys pressed in normal mode until action is run
       :showtabline 2 ;; Show the tabline even when just one tab is open
       :signcolumn :yes ;; Always show the sign column beside the number (see number)
       :smartindent true ;; Ident new lines in a smart way (see autoindent)
       :smarttab true ;; Treat spaces as tabs in increments of shiftwidth
       :softtabstop 0 ;; Do not insert spaces when pressing tab (see shiftwidth)
       :splitbelow true ;; Open splits below the current window
       :splitright true ;; Open splits right of the current window
       :tabstop 4 ;; Number of columns to move when pressing <TAB> (see expandtab)
       :termguicolors true ;; Enable 24-bit RGB color in the TUI
       :timeoutlen 500 ;; Milliseconds to wait before completing a mapped sequence
       :updatetime 300 ;; Milliseconds to wait before writing to swap file
       :wildignorecase true ;; Ignore case in filenames browsed by wildmenu
       :wrap false}) ;; Do not wrap text that reaches the window's width


(tset vim.g :mapleader ",") ;; Set leader key to comma to call which-key
(tset vim.g :tex_flavor :latex) ;; Treat all .tex files as LaTeX instead of TeX
(vim.opt.shortmess:append :c)
