(import-macros {: augroup!
                : autocmd!} :fnl/macros/autocmd)


(let [au (augroup! :filetype-behaviour)]
  (autocmd! au :FileType
            {:desc "Remove colorcolumn in specific files"
             :pattern [:fugitive* :git]
             :callback (fn [] (tset vim.opt_local :colorcolumn ""))}))


(let [au (augroup! :misc)]
  (autocmd! au :TextYankPost
            {:desc "Highlight yanked text for `timeout` milliseconds"
             :pattern "*"
             :callback (fn [] (vim.highlight.on_yank
                         {:higroup :IncSearch
                          :timeout 400
                          :on_visual true}))})

  (autocmd! au :TermOpen
            {:desc "Unlist terminal buffers"
             :pattern "*"
             :callback (fn [] (set vim.opt_local.buflisted false))}))


(let [au (augroup! :prose)]
  (autocmd! au :FileType
            {:desc "Wrap text in prose files"
             :pattern [:markdown :tex]
             :callback (fn [] (set vim.opt_local.wrap true))}))


(let [au (augroup! :quit)]
  (autocmd! au :FileType
            {:desc "Press `q` to quit secondary buffers"
             :pattern [:checkhealth :fugitive :git* :help :lspinfo]
             :callback (fn []
                         ;; (vim.api.nvim_win_close 0 true) ;; TODO: Replace vim command with this
                         (vim.api.nvim_buf_set_keymap
                           0 :n :q :<cmd>close!<cr>
                           {:noremap true :silent true}))}))


(local au-saving (augroup! :saving-files))
(autocmd! au-saving [:BufWritePre :FileWritePre]
          {:desc "Create parent directories before saving files"
           :pattern "*"
           :command "silent! call mkdir(expand(\"<afile>:p:h\"), \"p\")"})


(if _G.GROCTEL_WORK_PC
    (let [au (augroup! :work-pc)]
        (autocmd! au :FileType
                  {:desc "Fill branch name on commit for Jira integration"
                   :pattern [:gitcommit :NeogitCommitMessage]
                   :callback (fn []
                               (let [vim-macro
                                      (vim.api.nvim_replace_termcodes
                                       "/On branch<cr>:nohl<cr>2WYggP0df/A:<space>"
                                       true false true)]
                                 (vim.api.nvim_feedkeys vim-macro :n true)))}))

    (autocmd! au-saving :BufWritePre
              {:desc "Remove extra spaces and lines before saving files"
               :pattern "*"
               :command (.. "let current_pos = getpos(\".\") "
                         "| silent! %s/\\v\\s+$|\\n+%$//e "
                         "| silent! call setpos(\".\", current_pos)")}))
