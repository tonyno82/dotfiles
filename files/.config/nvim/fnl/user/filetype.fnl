(import-macros {: augroup!
                : autocmd!} :fnl/macros/autocmd)

(let [au (augroup! :filetype-by-extension)]
  (each [filetype extensions (pairs {:cpp :*.tpp
                                     :glsl [:*.comp
                                            :*.frag
                                            :*.fs
                                            :*.geom
                                            :*.tesc
                                            :*.tese
                                            :*.vert
                                            :*.vs]
                                     :tex :*.tikz})]
    (autocmd! au [:BufNewFile :BufRead]
              {:desc "Set filetype depending on file extension"
               :pattern extensions
               :command (.. "set filetype=" filetype)})))
