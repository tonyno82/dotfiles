(import-macros {: plug!} :fnl/macros/plugins)


[(plug! :goerz/jupytext.vim)
 (plug! :lervag/vimtex
        {:lazy true
         ;; WARN: Requires lots of work to setup
         ;; :dependencies :jbyuki/nabla.nvim
         :ft :tex})]
