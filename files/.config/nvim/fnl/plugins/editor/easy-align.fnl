(local keymap_set vim.keymap.set)
(local maps [[:ga "<Plug>(EasyAlign)"]
             [:gA "<Plug>(LiveEasyAlign)"]])

(for [i 1 (length maps)]
  (let [map (. maps i)]
    (keymap_set :n (. map 1) (. map 2) {:noremap true :silent true})
    (keymap_set :x (. map 1) (. map 2) {:noremap true :silent true})))
