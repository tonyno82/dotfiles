(import-macros {: plug!} :fnl/macros/plugins)


[(plug! :chaoren/vim-wordmotion)
 (plug! :matze/vim-move)
 (plug! :wellle/targets.vim)

 (plug! :hrsh7th/nvim-cmp
        {:dependencies [:hrsh7th/cmp-buffer
                        :hrsh7th/cmp-cmdline
                        :hrsh7th/cmp-latex-symbols
                        :hrsh7th/cmp-path
                        :onsails/lspkind.nvim
                        (plug! :hrsh7th/cmp-nvim-lsp
                               {:dependencies :neovim/nvim-lspconfig})
                        (plug! :saadparwaiz1/cmp_luasnip
                               {:dependencies :L3MON4D3/LuaSnip
                               :config (fn [] (require :plugins.editor.luasnip))})]
         :config (fn [] (require :plugins.editor.cmp))})

 (plug! :junegunn/vim-easy-align
        {:lazy true
         :keys [:ga :gA]
         :config (fn [] (require :plugins.editor.easy-align))})

 (plug! :mattn/emmet-vim
        {:config (fn [] (require :plugins.editor.emmet))})

 (plug! :nmac427/guess-indent.nvim
        {:config (fn [] ((. (require :guess-indent) :setup) {}))})

 (plug! :numToStr/Comment.nvim
        {:config (fn [] (require :plugins.editor.comment))})

 ;; Repetable plugins grouped for convenience
 (plug! :tpope/vim-repeat
        {:dependencies [:AndrewRadev/tagalong.vim
                    :inkarkat/vim-ReplaceWithRegister
                    :tpope/vim-surround]})

 (plug! :weilbith/nvim-code-action-menu
        {:lazy true
         :cmd :CodeActionMenu})]
