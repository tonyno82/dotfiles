(local luasnip (require :luasnip))
(luasnip.setup {:update_events "TextChanged,TextChangedI"})

(each [_ filetype (pairs [:c
                          :python
                          :tex])]
  (require (.. :plugins.editor.luasnip. filetype)))
(luasnip.filetype_extend :cpp [:c])


(local keymap_set vim.keymap.set)
(local keymap_opts {:noremap true})

(macro map! [mode keys action]
  `(keymap_set ,mode ,keys ,action keymap_opts))

(macro imap! [keys action] `(map! :i ,keys ,action))
(macro smap! [keys action] `(map! :s ,keys ,action))

(imap! :<C-B> (fn [] (luasnip.jump 1)))
(imap! :<C-Z> (fn [] (luasnip.jump -1)))
(smap! :<C-B> (fn [] (luasnip.jump 1)))
(smap! :<C-Z> (fn [] (luasnip.jump -1)))
