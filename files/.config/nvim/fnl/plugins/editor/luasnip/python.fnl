;; NOTE: Boilerplate code that should be macroed away, but I can't do it :(

(local ls (require :luasnip))
(local ls-insert ls.insert_node)
(local ls-snippet ls.snippet)
(local ls-function ls.function_node)
(local ls-text ls.text_node)

(fn livecopy [text]
  (. (. text 1) 1))

(macro snip! [...] `(ls-snippet ,...))
(macro f! [...] `(ls-function ,...))
(macro i! [...] `(ls-insert ,...))
(macro t! [...] `(ls-text ,...))

(macro snippets! [filetype ...]
  `(ls.add_snippets ,filetype ,...))

;; END BOILERPLATE

(snippets! :python
  [(snip! {:trig :def
           :dscr "Function, optionally member, definition"}
          [(t! "def ") (i! 1) (t! " (") (i! 2 "self,") (t! "):")
           (t! ["" "    "]) (i! 0)])

   (snip! {:trig :p
           :dscr "Print function"}
          [(t! "print(") (i! 1) (t! ")")
           (t! "") (i! 0)])])
