;; NOTE: Boilerplate code that should be macroed away, but I can't do it :(

(local ls (require :luasnip))
(local ls-insert ls.insert_node)
(local ls-snippet ls.snippet)
(local ls-function ls.function_node)
(local ls-text ls.text_node)

(fn livecopy [text]
  (. (. text 1) 1))

(macro snip! [...] `(ls-snippet ,...))
(macro f! [...] `(ls-function ,...))
(macro i! [...] `(ls-insert ,...))
(macro t! [...] `(ls-text ,...))

(macro snippets! [filetype ...]
  `(ls.add_snippets ,filetype ,...))

;; END BOILERPLATE

(snippets! :c
  [(snip! {:trig :def
           :dscr "Preprocessor symbol definition"}
          [(t! "#define ") (i! 1)])

   (snip! {:trig :i
           :dscr "Preprocessor file import"}
          [(t! "#include \"") (i! 1) (t! "\"")])

   (snip! {:trig :ifndef
           :dscr "Preprocessor import guard"}
          [(t! "#ifndef ") (i! 1)
           (t! ["" "#define "]) (f! livecopy [1])
           (t! ["" "" ""]) (i! 0)
           (t! ["" "" "" "#endif"])])

   (snip! {:trig :it
           :dscr "Preprocessor system file import"}
          [(t! "#include <") (i! 1) (t! ">")])

   (snip! {:trig :main
           :dscr "Main function basic structure"}
          [(t! "int main (") (i! 1 "int argc, char ** argv")
           (t! [")" "{" "\t"]) (i! 2)
           (t! ["" "" "\treturn 0;" "}"])])])

;; -- snippet class
;; -- class ${1:Name}
;; -- {
;; -- private:
;; -- 	$2

;; -- public:
;; -- 	$3
;; -- };
;; -- endsnippet

;; -- snippet for+
;; -- for (int $1 = 0; $1 < $2; $1++)
;; -- {
;; -- 	$3
;; -- }
;; -- endsnippet

;; -- snippet for-
;; -- for (int $1 = $2; $1 > 0; $1--)
;; -- {
;; -- 	$3
;; -- }
;; -- endsnippet

;; -- snippet forit
;; -- for (auto it = $1.begin(); it != $1.end(); ++it)
;; -- 	$2
;; -- endsnippet

;; -- snippet forcit
;; -- for (auto it = $1.cbegin(); it != $1.cend(); ++it)
;; -- 	$2
;; -- endsnippet

;; snippet /**
;; /** $1
;;  *
;;  * $2
;;  */
;; endsnippet

;; snippet /class
;; /** @class $1
;;  *
;;  * $2
;;  */
;; endsnippet

;; snippet /enum
;; /** @enum $1
;;  *
;;  * $2
;;  */
;; endsnippet

;; snippet /file
;; /** @file $1
;;  */
;; endsnippet

;; snippet /fn
;; /** @fn $1
;;  *
;;  * $2
;;  */
;; endsnippet
