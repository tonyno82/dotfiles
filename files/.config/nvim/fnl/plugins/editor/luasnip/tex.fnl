;; NOTE: Boilerplate code that should be macroed away, but I can't do it :(

(local ls (require :luasnip))
(local ls-insert ls.insert_node)
(local ls-snippet ls.snippet)
(local ls-function ls.function_node)
(local ls-text ls.text_node)

(fn livecopy [text]
  (. (. text 1) 1))

(macro snip! [...] `(ls-snippet ,...))
(macro f! [...] `(ls-function ,...))
(macro i! [...] `(ls-insert ,...))
(macro t! [...] `(ls-text ,...))

(macro snippets! [filetype ...]
  `(ls.add_snippets ,filetype ,...))

;; END BOILERPLATE

(macro begin-end! [env name]
  `(snip! {:trig ,name
           :descr (.. "Encloses the `" ,name "` environment")}
          [(t! (.. "\\begin{" ,env "}"))
           (t! ["" "\t"]) (i! 0)
           (t! ["" (.. "\\end{" ,env "}")])]))

(macro begin-end!! [env]
  `(begin-end! ,env ,env))


(snippets! :tex
  [(begin-end! :displayquote :quote)
   (begin-end! :tikzpicture :tikz)
   (begin-end!! :array)
   (begin-end!! :center)
   (begin-end!! :enumerate)
   (begin-end!! :itemize)

   (snip! {:trig "$$"
           :descr "Maths block"}
          [(t! ["\\[" "\t"]) (i! 1) (t! ["" "\\]"])])

   (snip! {:trig :begin
           :dscr "Environment enclosure"}
          [(t! "\\begin{") (i! 1) (t! "}")
           (t! ["" "\t"]) (i! 0)
           (t! ["" "\\end{"]) (f! livecopy [1]) (t! "}")])

   (snip! {:trig :bf
           :dscr "Bold-face text"}
          [(t! "\\textbf{") (i! 1) (t! "}")
           (i! 0)])

   (snip! {:trig :br
           :dscr "Page break"}
          [(t! "\\pagebreak")
           (i! 0)])

   (snip! {:trig :chap
           :dscr "Chapter"}
          [(t! "\\chapter{") (i! 1) (t! "}")
           (i! 0)])

   (snip! {:trig :figure
           :dscr "Captioned figure"}
          [(t! "\\begin{figure}")
           (t! ["" "\t"]) (i! 1)
           (t! ["" "\t\\caption{"]) (i! 2) (t! "}")
           (t! ["" "\\end{figure}" ""]) (i! 0)])

   (snip! {:trig :foot
           :dscr "Footnote linked from preceding word"}
          [(t! "\\footnote{") (i! 1) (t! "}") (i! 0)])

   (snip! {:trig :input
           :dscr "TeX file inline import"}
          [(t! "\\input{") (i! 1) (t! "}")
           (i! 0)])

   (snip! {:trig :it
           :dscr "Italics-face text"}
          [(t! "\\textit{") (i! 1) (t! "}")
           (i! 0)])

   (snip! {:trig :item
           :dscr "Item inside `itemize` environment"}
          [(t! "\\item")
           (t! ["" "\t"]) (i! 1)])

   (snip! {:trig :item$
           :dscr "Maths item inside `itemize` environment"}
          [(t! "\\item")
           (t! ["" "\t$"]) (i! 1) (t! "$") (i! 2)])

   (snip! {:trig :itembf
           :dscr "Bold-faced-title item inside `itemize` environment"}
          [(t! "\\item")
           (t! ["" "\t\\textbf{"]) (i! 1) (t! ":}")
           (t! ["" "\t"]) (i! 2)])

   (snip! {:trig :itemtt
           :dscr "Teletype-faced-title item inside `itemize` environment"}
          [(t! "\\item")
           (t! ["" "\t\\textttt{"]) (i! 1) (t! "}\\textbf{:}")
           (t! ["" "\t"]) (i! 2)])

   (snip! {:trig :list
           :dscr "Listing with language-specific syntax"}
          [(t! "\\begin{lstlisting}[language=") (i! 1) (t! "]")
           (t! "") (i! 2)
           (t! ["" "\\end{lstlisting}" ""]) (i! 0)])

   (snip! {:trig :listt
           :dscr "Listing with default syntax"}
          [(t! "\\begin{lstlisting}")
           (t! "") (i! 1)
           (t! ["" "\\end{lstlisting}" ""]) (i! 0)])

   (snip! {:trig :req
           :dscr "LaTeX package inline import inside package"}
          [(t! "\\RequirePackage{") (i! 1) (t! "}")])

   (snip! {:trig :sec
           :dscr "Section"}
          [(t! "\\section{") (i! 1) (t! "}")
           (i! 0)])

   (snip! {:trig :ssec
           :dscr "Subsection"}
          [(t! "\\subsection{") (i! 1) (t! "}")
           (i! 0)])

   (snip! {:trig :sssec
           :dscr "Subsubsection"}
          [(t! "\\subsubsection{") (i! 1) (t! "}")
           (i! 0)])

   (snip! {:trig :tabular
           :dscr "Basic table environment"}
          [(t! "\\begin{tabular}{") (i! 1) (t! "}")
           (t! "") (i! 2)
           (t! ["" "\\end{tabular}"])])

   (snip! {:trig :url
           :dscr "Clickable, teletype-faced URL"}
          [(t! "\\url{") (i! 1) (t! "}")])

   (snip! {:trig :use
           :dscr "LaTeX package inline import"}
          [(t! "\\usepackage{") (i! 1) (t! "}")])

   (snip! {:trig :tt
           :dscr "Teletype-(monospaced)-face text"}
          [(t! "\\texttt{") (i! 1) (t! "}")
           (i! 0)])])

;; -- snippet '"([^"]*)"' "LaTeX-style quotes" rwA
;; -- \`\``!p snip.rv=match.group(1)`''$0
;; -- endsnippet

;; snippet 'chap(\*?)' "Chapter" br
;; \chapter`!p snip.rv=match.group(1)`{$0}
;; endsnippet

;; snippet 'part(\*?)' "Part" br
;; \part`!p snip.rv=match.group(1)`{$0}
;; endsnippet

;; snippet '(s*)sec(\*?)' "Section and subs" br
;; `!p snip.rv="\\" + "sub"*len(match.group(1)) + "section" + match.group(2)`{$0}
;; endsnippet
