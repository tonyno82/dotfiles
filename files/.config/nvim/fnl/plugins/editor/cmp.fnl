(local cmp (require :cmp))
(local mapping cmp.mapping)
(local luasnip (require :luasnip))
(local lspkind (require :lspkind))
(local lspkind-format ((. lspkind :cmp_format) {:mode :symbol_text
                                                :maxwidth 50}))


(fn format-completion-entry [entry vim-item]
  (let [kind (lspkind-format entry vim-item)
        item-info (vim.split kind.kind "%s" {:trimempty true})]
    (tset vim-item :abbr (vim.trim vim-item.abbr))
    (tset vim-item :kind (.. " " (or (. item-info 1) "") " "))
    (tset vim-item :menu (-> item-info (and (.. "(" (. item-info 2) ")")) (or "")))
    vim-item))


(cmp.setup
  {:formatting {:fields [:kind :abbr :menu]
                :format format-completion-entry}
   :mapping (mapping.preset.insert
     {:<C-d> (mapping (mapping.scroll_docs 4)  [:i :c])
      :<C-u> (mapping (mapping.scroll_docs -4) [:i :c])
      :<CR> (fn []
              (vim.api.nvim_feedkeys "\n" :i true)
              (mapping.close))
      :<Tab> (mapping.confirm {:behavior cmp.ConfirmBehavior.Insert
                               :select true})})
   :snippet {:expand (fn [args] (luasnip.lsp_expand args.body))}
   :sorting {:priority_weight 100}
   :sources [{:name :path}
             {:name :luasnip}
             {:name :nvim_lsp}
             {:name :latex_symbols}
             {:name :treesitter}
             {:name :buffer}]
   :view {:entries :custom
          :selection_order :near_cursor}
   :window {:completion {:winhighlight "Normal:Pmenu,FloatBorder:Pmenu,Search:None"
                         :col_offset -3
                         :side_padding 0}}})


(cmp.setup.cmdline ":"
                   {:sources [{:name :cmdline
                               :group_index 1}
                             {:name :cmdline_history
                              :group_index 2}]
                   :mapping (cmp.mapping.preset.cmdline [])})
                   ;; :view {:entries native}


(cmp.setup.cmdline "/"
                   {:sources [{:name :buffer}]
                    :mapping (cmp.mapping.preset.cmdline [])})
                    ;; :view {:entries native}
