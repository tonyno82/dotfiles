((. (require :nvim-treesitter.configs) :setup)
  {:sync_install true
   :auto_install true
   :highlight {:enable true
               :disable [:bash
                         :help
                         :latex
                         :vim]
               :additional_vim_regex_highlighting false}
   :textobjects {:select {:enable true
                          :lookahead true
                          :keymaps {:af "@function.outer"
                                    :if "@function.inner"
                                    :ac "@class.outer"
                                    :ic "@class.inner"}}}})
