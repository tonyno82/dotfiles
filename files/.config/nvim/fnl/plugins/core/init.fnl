(import-macros {: plug!} :fnl/macros/plugins)


[(plug! :lewis6991/impatient.nvim)
 (plug! :nvim-lua/plenary.nvim)
 (plug! :folke/lazy.nvim)
 (plug! :kyazdani42/nvim-web-devicons)

 (plug! :andweeb/presence.nvim
        {:config (fn []
                   (tset vim.g :presence_auto_update 1)
                   (tset vim.g :presence_neovim_image_text :gitlab.com/groctel))})

 (plug! :anuvyklack/hydra.nvim
        {:config (fn [] (require :plugins.core.hydra))})

 (plug! :linty-org/readline.nvim
        {:lazy true
         :event :CmdlineEnter
         :config (fn [] (require :plugins.core.readline))})

 ;; (plug! :$HOME/Documents/Git/Fork/neorg/
 (plug! :nvim-neorg/neorg
        {:lazy true
         :dependencies [:nvim-lua/plenary.nvim
                        :nvim-neorg/neorg-telescope
                        :nvim-treesitter/nvim-treesitter]
         :ft :norg
         :build ":Neorg sync-parsers"
         :config (fn [] (require :plugins.core.neorg))})

 (plug! :nvim-telescope/telescope.nvim
        {:lazy true
         :dependencies [:kyazdani42/nvim-web-devicons
                        :nvim-telescope/telescope-ui-select.nvim]
         :cmd :Telescope
         :config (fn [] (require :plugins.core.telescope))})

 (plug! :nvim-treesitter/nvim-treesitter
        {:dependencies :nvim-treesitter/playground
                       ; :nvim-treesitter/nvim-treesitter-textobjects]
         :build [":TSInstall all"
                 ":TSUpdate"]
         :config (fn [] (require :plugins.core.treesitter))})]
