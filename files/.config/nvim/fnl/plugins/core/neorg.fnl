(local setup {
  :load {:core.defaults []
         :core.norg.concealer []
         :core.norg.dirman {:config {:autochdir true
                                     ;; :default_workspace :root
                                     :open_last_workspace false}}
         :core.norg.journal {:config {:workspace :journal}}
         ;; :core.gtd.base {:config {:workspace :root}}
         :core.norg.qol.toc []}})
         ;; :core.integrations.telescope] []


(local neorg-root "~/.local/norg")

(if _G.GROCTEL_WORK_PC
    (tset (. setup :load :core.norg.dirman :config) :workspaces
          {:root neorg-root
           :tasks (.. neorg-root "/Tasks")})
    (tset (. setup :load :core.norg.dirman :config) :workspaces
          {:root neorg-root
           :home (.. neorg-root "/Home")
           :journal (.. neorg-root "/Journal")
           :ugr (.. neorg-root "/UGR")}))


((. (require :neorg) :setup) setup)
