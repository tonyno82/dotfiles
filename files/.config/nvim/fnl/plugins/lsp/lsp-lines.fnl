(local lines (require :lsp_lines))
(lines.setup)

(var first_call true)

(vim.api.nvim_create_user_command :LSPLinesToggle
  ;; HACK: Open PR to remove this requirement (allow toggle when loading)
  (fn []
    (when first_call
      (lines.toggle)
      (set first_call false))
    (vim.diagnostic.config {:virtual_text (not (lines.toggle))}))
  {})
