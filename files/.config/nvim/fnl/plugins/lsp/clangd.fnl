((. (require :clangd_extensions) :setup) {
  :server {:cmd [:clangd
                 :--all-scopes-completion
                 :--background-index
                 :--clang-tidy
                 :--completion-style=detailed
                 :--function-arg-placeholders
                 :--header-insertion=iwyu
                 :--header-insertion-decorators
                 :--limit-results=0
                 :--log=verbose
                 :--malloc-trim
                 :--pch-storage=memory
                 :--pretty
                 :--suggest-missing-includes]
           :on_attach
               (fn [client bufnr]
                 (tset client.server_capabilities :semanticTokensProvider nil))}
  :extensions {:ast {:role_icons {:declaration :⯆
                                  :expression :│
                                  :specifier :
                                  :statement :⯈
                                  :type :λ
                                  "template argument" :}}
               :inlay_hints {:only_current_line true}
               :symbol_info {:border :solid}}})
