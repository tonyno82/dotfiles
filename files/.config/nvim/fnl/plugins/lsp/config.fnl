(local lsp (require :lspconfig))
;; (local navic (require :nvim-navic))

(macro setup-servers! [servers]
  (icollect [server setup (pairs servers) &into `(do)]
    `((. lsp ,server :setup) ,setup)))

(setup-servers! {:bashls []
                 :cmake []
                 :cssls []
                 :html []
                 :intelephense []
                 :pyright []
                 :rust_analyzer []
                 :texlab []
                 :tsserver []
                 :vimls []

                 :sumneko_lua {:settings {:Lua
                    {:diagnostics {:globals [:_G
                                             :packer_plugins
                                             :vim]}
                     :runtime {:version :LuaJIT}
                     :telemetry {:enable false}
                     :workspace {:library (vim.api.nvim_get_runtime_file "" true)}}}}})

(vim.cmd "sign define DiagnosticSignError text=  texthl=DiagnosticSignError
          sign define DiagnosticSignWarn  text=  texthl=DiagnosticSignWarn
          sign define DiagnosticSignInfo  text=  texthl=DiagnosticSignInfo
          sign define DiagnosticSignHint  text=  texthl=DiagnosticSignHint")

((. (require :fidget) :setup) {:text {:spinner :arc}})
