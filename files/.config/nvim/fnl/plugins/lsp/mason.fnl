((. (require :mason) :setup)
  {:pip {:upgrade_pip true}
   :ui {:icons {:package_installed ""
                :package_pending ""
                :package_uninstalled ""}}})

((. (require :mason-tool-installer) :setup)
  {:auto_update true
   :ensure_installed [;; DAP
                      :debugpy
                      ;; LSP
                      :bash-language-server
                      :clangd
                      :cmake-language-server
                      :css-lsp
                      :dockerfile-language-server
                      :html-lsp
                      :intelephense
                      :json-lsp
                      :lua-language-server
                      :marksman
                      :pyright
                      :rust-analyzer
                      :texlab
                      :typescript-language-server
                      :vim-language-server]})
