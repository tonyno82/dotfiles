(import-macros {: plug!} :fnl/macros/plugins)


[(plug! :neovim/nvim-lspconfig
      {:dependencies [:j-hui/fidget.nvim
                        :p00f/clangd_extensions.nvim]
         :config (fn []
                   (require :plugins.lsp.clangd)
                   (require :plugins.lsp.config))})

 (plug! :mfussenegger/nvim-dap
        {:dependencies [:rcarriga/nvim-dap-ui]
         :config (fn [] (require :plugins.lsp.dap))})

 (plug! :onsails/lspkind.nvim
        {:config (fn [] (require :plugins.lsp.lspkind))})

 (plug! :williamboman/mason.nvim
        {:dependencies [:WhoIsSethDaniel/mason-tool-installer.nvim]
         :config (fn [] (require :plugins.lsp.mason))})

 (plug! "~whynothugo/lsp_lines.nvim"
        {:lazy true
         :url "https://git.sr.ht/~whynothugo/lsp_lines.nvim"
         :cmd :LSPLinesToggle
         :config (fn [] (require :plugins.lsp.lsp-lines))})]
