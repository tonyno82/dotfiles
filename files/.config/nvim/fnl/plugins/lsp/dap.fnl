(local dap (require :dap))

(tset dap.adapters :python
  {:type :executable
   :command (.. (vim.fn.stdpath :data) :/mason/packages/debugpy/venv/bin/python)
   :args [:-m :debugpy.adapter]})

(tset dap.configurations :python
  [{:type :python
    :request :launch
    :name "Launch file"

    ;; https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings
    :program "${file}"}])


((. (require :dapui) :setup)
  {:controls {:icons {:pause " "
                      :play " "
                      :step_into " "
                      :step_over " "
                      :step_out " "
                      :step_back " "
                      :run_last "↺ "
                      :terminate "⯀ "}}})
