(local hlslens (require :hlslens))

(local keymap-set vim.keymap.set)
(local keys [:n :N :* :# :g* :g#])
(local map-opts {:noremap true :silent true})
(local normal vim.cmd.normal)


(for [i 1 (length keys) 1]
  (keymap-set :n (. keys i)
              (fn []
                (when (pcall normal {:args [(. keys i)] :bang true})
                  (hlslens.start)))
              map-opts))

((. hlslens :setup) {:auto-enable false})
