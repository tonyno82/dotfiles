(import-macros {: plug!} :fnl/macros/plugins)


[(plug! :Eandrju/cellular-automaton.nvim)

 (plug! :akinsho/toggleterm.nvim
        {:config (fn [] (require :plugins.ui.toggleterm))})

 (plug! :feline-nvim/feline.nvim
        {:dependencies [:SmiteshP/nvim-gps
                        ;; :SmiteshP/nvim-navic
                        :kyazdani42/nvim-web-devicons]
         :config (fn [] (require :plugins.ui.feline))})

 (plug! :folke/todo-comments.nvim
        {:config (fn [] ((. (require :todo-comments) :setup) {}))})

 (plug! :folke/tokyonight.nvim
        {:config (fn [] (require :plugins.ui.tokyonight))
         :priority 1000})

 (plug! :folke/trouble.nvim
        {:lazy true
         :dependencies [:kyazdani42/nvim-web-devicons]
         :cmd :TroubleToggle})

 (plug! :folke/which-key.nvim
        {:config (fn [] (require :plugins.ui.which-key))})

 (plug! :junegunn/goyo.vim
        {:lazy true
         :cmd :Goyo
         :dependencies [:junegunn/limelight.vim]})

 (plug! :kevinhwang91/nvim-hlslens
        {:config (fn [] (require :plugins.ui.nvim-hlslens))})

 (plug! :kosayoda/nvim-lightbulb
        {:config (fn [] (require :plugins.ui.lightbulb))})

 (plug! :kyazdani42/nvim-tree.lua
        {:lazy true
         :cmd :NvimTreeToggle
         :config (fn [] (require :plugins.ui.nvim-tree))})

 (plug! :mbbill/undotree
        {:lazy true
         :cmd :UndotreeToggle
         :config (fn []
                   (tset vim.g :undotree_SetFocusWhenToggle 1)
                   (tset vim.g :undotree_ShortIndicators 1)
                   (tset vim.g :undotree_TreeNodeShape :◯)
                   (tset vim.g :undotree_WindowLayout 2))})

 (plug! :noib3/nvim-cokeline
        {:dependencies [:kyazdani42/nvim-web-devicons]
         :config (fn [] (require :plugins.ui.cokeline))})

 (plug! :norcalli/nvim-colorizer.lua
        {:config (fn [] (require :plugins.ui.colorizer))})

 (plug! :petertriho/nvim-scrollbar
        {:dependencies [:kevinhwang91/nvim-hlslens]
         :config (fn [] (require :plugins.ui.nvim-scrollbar))})

 (plug! :rcarriga/nvim-notify
        {:config (fn [] (require :plugins.ui.notify))})]
