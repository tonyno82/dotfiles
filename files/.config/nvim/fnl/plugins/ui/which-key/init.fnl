(local wk (require :which-key))
(local keytree (require :plugins.ui.which-key.tree))

(wk.setup {:triggers :auto
           :plugins {:spelling {:enabled true
                                :suggestions 20}}})
(for [i 1 8 1]
  (local number (tostring i))
  (tset keytree :number [(.. "<cmd>BufferGoto " number :<cr>) (.. :Buffer number)]))

(local visual_keytree
  {:s {:name :+Sort...
       :s [":'<,'>sort\n"    "Sort"]
       :u [":'<,'>sort u\n"  "Sort uniq"]}})

(wk.register keytree {:prefix :<leader>})
(wk.register visual_keytree {:prefix :<leader>
                             :mode :v})
