(var diagnostics-active true)
(fn toggle-diagnostics []
  (set diagnostics-active (not diagnostics-active))
  (if diagnostics-active
        (vim.diagnostic.show)
    (vim.diagnostic.hide)))


(fn terminal [direction]
  (->
    (require :toggleterm.terminal)
    (. :Terminal)
    (: :new {:direction direction})
    (: :toggle)))


{:b ["<cmd>Telescope buffers<cr>"  :Buffers]
 :c [:<cmd>ColorizerToggle<cr>     "Toggle colours"]
 :d (require :plugins.ui.which-key.dap)
 :f ["<cmd>Telescope find_files<cr>"  "Find file"]
 :F ["<cmd>NvimTreeToggle .<cr>"      "File tree"]
 :g (require :plugins.ui.which-key.git)
 :h [":nohls<cr>"  "Disable highlight"] ;; Use `:` instead of `<cmd>` to let hlslens automatically detect the command
 :H ["<cmd>Telescope command_history<cr>"  "Command history"]
 :j {:name "+Jump to..."
     :d ["<cmd>Telescope lsp_definitions<cr>"  :Definition]
     :h [:<cmd>ClangdSwitchSourceHeader<cr>    "Header/Source"]
     :s ["<cmd>Telescope live_grep<cr>"        "Arbitrary string"]
     :S ["<cmd>Telescope grep_string<cr>"      "String below cursor"]}
 :l {:name :+LSP...
   :a [:<cmd>CodeActionMenu<cr>            "Code actions"]
   :d [:<cmd>LSPLinesToggle<cr>            "Toggle diagnostics lines"]
   :D [toggle-diagnostics                  "Toggle diagnostics"]
   :i ["<cmd>lua vim.lsp.buf.hover()<cr>"  "Symbol info"]
   :I [:<cmd>LspInfo<cr>                   "Server info"]
   :M [:<cmd>Mason<cr>                     :Mason]
   :R [:<cmd>LspRestart<cr>                :Restart]
   :s [:<cmd>LspStart<cr>                  :Start]
   :S [:<cmd>LspStop<cr>                   :Stop]
   :t [:<cmd>TroubleToggle<cr>             :Trouble]}
 :m ["<cmd>Telescope man_pages<cr>"  "Man pages"]
 :n {:name :+Neorg...
   :g ["<cmd>Neorg gtd views<cr>"  "GTD views"]
   :G {:name :+GTD...
     :c ["<cmd>Neorg gtd capture<cr>"  "Create task"]
     :e ["<cmd>Neorg gtd edit<cr>"     "Edit task"]}}
 :s {:name :+Spelling...
   :s  {1 ":setlocal spell spelllang="  2 :Spelling           :silent false}
   :S  {1 ":set spell spelllang="       2 "Spelling (global)" :silent false}
   " " {1 "]szzz="                      2 "Next mistake"      :silent false}} ;; TODO: Trigger WK here
 :t {:name :+Terminal...
   :t [(fn [] (terminal :float))       "Open floating terminal"]
   :T [:<cmd>ToggleTermToggleAll<cr>   "Open terminal in buffer"]
   :s [(fn [] (terminal :horizontal))  "Split to terminal"]
   :v [(fn [] (terminal :vertical))    "Vsplit to terminal"]}
 :T [:<cmd>TodoTelescope<cr>   "TODO appearances"]
 :u [:<cmd>UndotreeToggle<cr>  :UndoTree]
 :v {:name :+Vim...
   :d ["<cmd>Telescope find_files cwd=~/.config/nvim/<cr>"  "Dotfiles"]
   :s ["<cmd>source $MYVIMRC<cr>"                 "Source vimrc"]
   :h [:<cmd>checkhealth<cr>                      "Check nvim health"]
   :l ["<cmd>CellularAutomaton game_of_life<cr>"  "Game of life"]
   :r ["<cmd>CellularAutomaton make_it_rain<cr>"  "Make it rain!"]}
 :z ["<cmd>Goyo | Limelight!! | Gitsigns toggle_signs<cr>"  "Zen mode"]}
