(local dap (require :dap))
(local dapui (require :dapui))

{:name :+DAP...
 :b {:name :+Breakpoints...
     :b [dap.toggle_breakpoint  "Toggle breakpoint"]
     :c [dap.clear_breakpoint   "Clear breakpoints"]}

 :c [dap.continue   "Continue"]
 :n [dap.step_over  "Next (step over)"]
 :u [dapui.toggle   "Toggle UI"]
 :s [dap.step_into  "Step into"]}

;; set_breakpoint({condition} {hit_condition} {log_message})
;;                                                           *dap.set_breakpoint()*

;;         Same as |toggle_breakpoint| but is guaranteed to overwrite previous
;;         breakpoint.

;; toggle_breakpoint({condition} {hit_condition} {log_message})
;;                                                        *dap.toggle_breakpoint()*

;;         Creates or removes a breakpoint at the current line.

;;         Parameters: ~
;;             {condition}     Optional condition that must be met for the debugger
;;                             to stop at the breakpoint.
;;             {hit_condition} Optional hit condition e.g. a number as a string
;;                             that tells how often this breakpoint should be visited
;;                             to stop.
;;             {log_message}   Optional log message. This transforms the breakpoint
;;                             into a log point. Variable interpolation with {foo} is
;;                             supported within the message.

;; list_breakpoints()                                     *dap.list_breakpoints()*

;;         Lists all breakpoints and log points in quickfix window.

;; set_exception_breakpoints({filters} {exceptionOptions})
;;                                               *dap.set_exception_breakpoints()*

;;     Sets breakpoints on exceptions filtered by `filters`. If `filters` is not
;;     provided it will prompt the user to choose from the available filters of the
;;     debug adapter.

;;     Parameters: ~
;;         {filters}          A list of exception types to stop on (optional).
;;                            Most debug adapters offer categories like `"uncaught"` and
;;                            `"raised"` to filter the exceptions.
;;                            If set to "default" instead of a table the
;;                            default options as recommended by the debug adapter are
;;                            used.
;;         {exceptionOptions} ExceptionOptions[]?
;;                            (https://microsoft.github.io/debug-adapter-protocol/specification#Types_ExceptionOptions)
;;
;;     >
;;         ;; Ask user to stop on which kinds of exceptions
;;         require'dap'.set_exception_breakpoints()
;;         ;; don't stop on exceptions
;;         require'dap'.set_exception_breakpoints({})
;;         ;; stop only on certain exceptions (debugpy offers "raised" "uncaught")
;;         require'dap'.set_exception_breakpoints({"uncaughted"})
;;         require'dap'.set_exception_breakpoints({"raised" "uncaught"})
;;         ;; use default settings of debug adapter
;;         require'dap'.set_exception_breakpoints("default")
;;     <

;;     You can also set the default value via a `defaults.fallback` table:

;;     >
;;         require('dap').defaults.fallback.exception_breakpoints  {'raised'}
;;     <

;;     Or per config/adapter type:

;;     >
;;         require('dap').defaults.python.exception_breakpoints  {'raised'}
;;     <

;;     In this example `python` is the type. This is the same type used in
;;     |dap-configuration| or the |dap-adapter| definition.



;; run({config})                                                        *dap.run()*
;;         Looks up a debug adapter entry for the given configuration and runs it.
;;         This is implicitly called by |dap.continue()| if no debug session is
;;         active.

;;         Most users will want to start debugging using |dap.continue()| instead
;;         of using `run()`.  `run()` is intended for nvim-dap extensions which
;;         create configurations dynamically for example to debug individual test
;;         cases.

;;         Parameters:
;;             {config}  |dap-configuration| to run


;; run_last()                                                      *dap.run_last()*
;;         Re-runs the last debug adapter / configuration that ran using
;;         |dap.run()|.


;; launch({adapter} {config})                                       *dap.launch()*
;;         Launch a new debug adapter and then initialize it with the given
;;         |dap-configuration|

;;         You typically do not want to call this directly but use
;;         |dap.continue()| or |dap.run()|

;;         Parameters: ~
;;             {adapter}   `Adapter` to launch see |dap-adapter| the `type` is
;;                         not required in this case.
;;             {config}    |dap-configuration|


;; terminate(terminate_opts disconnect_opts cb)                                           *dap.terminate()*
;;         Terminates the debug session.

;;         If the debug adapter doesn't support the `terminateRequest`
;;         capability this will instead call |dap.disconnect()| with
;;         `terminateDebugee  true`.

;;         Parameters: ~
;;             {terminate_opts}      Options for the `terminate` request.
;;                                   Defaults to empty.
;;                                   Not used if |dap.disconnect| is used.

;;             {disconnect_opts}     Opts for |dap.disconnect|
;;                                   Defaults to `{ terminateDebuggee  true }`

;;             {cb}                  Callback that is invoked once the session
;;                                   terminated or immediately if no session is
;;                                   active.

;; disconnect(opts cb)                                              *dap.disconnect()*

;;         disconnect asks the debug adapter to disconnect from the debuggee and
;;         to terminate the debug adapter.

;;         The client session may remain open if the debug adapter does not
;;         terminate. To ensure the session gets closed also call |dap.close()|.

;;         Requires an active session.

;;         Parameters: ~
;;             {opts}    Table with options for the disconnect request.
;;                       Defaults to `{ restart  false terminateDebuggee  null }`

;;             {cb}      Callback that is invoked once the session
;;                       disconnected or immediately if no session is active.


;; close()                                                            *dap.close()*
;;         Closes the current session.

;;         This does NOT terminate the debug adapter or debugee.
;;         You usually want to use either |dap.terminate()| or |dap.disconnect()|
;;         instead.


;; attach({adapter} {config})                                       *dap.attach()*
;;         Attach to a running debug adapter and then initialize it with the
;;         given |dap-configuration|

;;         You typically do not want to call this directly but use
;;         |dap.continue()| or |dap.run()|




;; step_out([{opts}])                                              *dap.step_out()*
;;         Requests the debugee to step out of a function or method if possible.

;;         For options see |step_into|.

;; step_back([{opts}]                                             *dap.step_back()*
;;         Steps one step back. Debug adapter must support reverse debugging.

;;         For {opts} see |step_into|.

;; pause({thread_id})                                                 *dap.pause()*
;;         Requests debug adapter to pause a thread. If there are multiple threads
;;         it stops `thread_id` from the optional parameter or asks the user which
;;         thread to pause.

;; reverse_continue()                                      *dap.reverse_continue()*
;;         Continues execution reverse in time until last breakpoint.
;;         Debug adapter must support reverse debugging.

;; up()                                                                  *dap.up()*
;;         Go up in current stacktrace without stepping.

;; down()                                                              *dap.down()*
;;         Go down in current stacktrace without stepping.


;; goto_({line})                                                      *dap.goto_()*
;;         Let the debugger jump to a specific line or line under cursor.
;;         This is an optional feature and not all debug adapters support it.

;;         The code between the current location and the goto target is not
;;         executed but skipped.

;;         Parameters: ~
;;             {line}  Line number or line under cursor if nil.


;; restart_frame()                                            *dap.restart_frame()*
;;         Restart execution of the current frame

;;         This is an optional feature and not all debug adapters support it.


;; run_to_cursor()                                            *dap.run_to_cursor()*
;;         Continues execution to the current cursor.

;;         This temporarily removes all breakpoints sets a breakpoint at the
;;         cursor resumes execution and then adds back all breakpoints again.


;; repl.open({winopts} {wincmd})                                 *dap.repl.open()*
;;         Open a REPL / Debug-console.

;;         Parameters: ~
;;             {winopts}  optional table which may include:
;;                         `height` to set the window height
;;                         `width` to set the window width
;;                         Any other key/value pair that will be treated as window
;;                         option.

;;             {wincmd} command that is used to create the window for
;;                      the REPL. Defaults to 'belowright split'


;;         The REPL can be used to evaluate expressions. A `omnifunc` is set to
;;         support completion of expressions. It supports the following special
;;         commands:

;;           .exit               Closes the REPL
;;           .c or .continue     Same as |dap.continue|
;;           .n or .next         Same as |dap.step_over|
;;           .into               Same as |dap.step_into|
;;           .into_target        Same as |dap.step_into{askForTargetstrue}|
;;           .out                Same as |dap.step_out|
;;           .up                 Same as |dap.up|
;;           .down               Same as |dap.down|
;;           .goto               Same as |dap.goto_|
;;           .scopes             Prints the variables in the current scopes
;;           .threads            Prints all threads
;;           .frames             Print the stack frames
;;           .capabilities       Print the capabilities of the debug adapter
;;           .b or .back         Same as |dap.step_back|
;;           .rc or
;;           .reverse-continue   Same as |dap.reverse_continue|

;;         You can customize the builtin command names or define your own
;;         custom commands by extending `dap.repl.commands`:
;;         >
;;           local repl  require 'dap.repl'
;;           repl.commands  vim.tbl_extend('force' repl.commands {
;;             ;; Add a new alias for the existing .exit command
;;             exit  {'exit' '.exit' '.bye'}
;;             ;; Add your own commands; run `.echo hello world` to invoke
;;             ;; this function with the text "hello world"
;;             custom_commands  {
;;               ['.echo']  function(text)
;;                 dap.repl.append(text)
;;               end
;;               ;; Hook up a new command to an existing dap function
;;               ['.restart']  dap.restart
;;             }
;;           }

;;         <


;; repl.toggle({winopts} {wincmd})                             *dap.repl.toggle()*
;;         Opens the REPL if it is closed otherwise closes it.

;;         See |dap.repl.open| for a description of the argument.


;; repl.close()                                                  *dap.repl.close()*
;;         Closes the REPL if it is open.


;; set_log_level(level)                                       *dap.set_log_level()*
;;         Sets the log level. Defaults to `INFO`  >

;;             :lua require('dap').set_log_level('TRACE')
;; <

;;         Available log levels:

;;           TRACE
;;           DEBUG
;;           INFO
;;           WARN
;;           ERROR

;;         The log file is in the |stdpath| `cache` folder.
;;         To print the location:  >

;;             :lua print(vim.fn.stdpath('cache'))
;; <
;;         The filename is `dap.log`


;; session()                                                        *dap.session()*
;;         Returns the current session or nil if no session exists.


;; status()
;;         Returns the status of the current debug session as text
;;         This is intended to be used within the statusline

;;         If no debug session is active the result is empty.
