{:name :+Git...
 :a {1 "<cmd>Git add %<cr>" 2 "git add" :silent false}
 :b ["<cmd>Git blame<cr>"               "git blame"]
 :B ["<cmd>Telescope git_branches<cr>"  "git branch"]
 :c ["<cmd>Neogit commit<cr>"           "git commit"]
 :C {:name :+Commits...
     :b ["<cmd>Telescope git_bcommits<cr>"  "Buffer local commits"]
     :C ["<cmd>Telescope git_commits<cr>"   "Commits history"]}
 :d ["<cmd>DiffviewOpen -- %<cr>"    "git diff"]
 :D ["<cmd>DiffviewOpen<cr>"         "git diff (all)"]
 :g ["<cmd>Neogit kind=split<cr>"    "Neogit"]
 :f ["<cmd>Telescope git_files<cr>"  "Inspect files"]
 :i ["<cmd>Git init<cr>"             "git init"]
 :l ["<cmd>Git log<cr>"              "git log"]
 :L ["<cmd>Git log --oneline --decorate --graph<cr>"  "git log graph"]
 :m {1 "<cmd>Merginal<cr>"  2 "git merge"  :silent false}
 :p ["<cmd>Neogit pull<cr>"  "git pull"]
 :P ["<cmd>Neogit push<cr>"  "git push"]
 :s {:name :+Signs...
    :D ["<cmd>Gitsigns toggle_deleted<cr>"       "Deleted lines"]
    :l ["<cmd>Gitsigns setqflist<cr>"            "List hunks"]
    :n ["<cmd>Gitsigns next_hunk<cr>"            "Next hunk"]
    :p ["<cmd>Gitsigns prev_hunk<cr>"            "Previous hunk"]
    :P ["<cmd>Gitsigns preview_hunk_inline<cr>"  "Preview hunk"]
    :s ["<cmd>Gitsigns stage_hunk<cr>"           "Stage hunk"]
    :S ["<cmd>Gitsigns undo_stage_hunk<cr>"      "Undo stage hunk"]
    :r ["<cmd>Gitsigns reset _hunk<cr>"          "Reset hunk"]}
 :S ["<cmd>Telescope git_status<cr>"  "git status"]
 :t ["<cmd>Telescope git_stash<cr>"   "git stash"]
 :w ["<cmd>Git whatchanged<cr>"       "git whatchanged"]
 :x {:name :+Conflicts...
    :b ["<cmd>GitConflictChooseBoth<cr>"    "Adopt both changes"]
    :i ["<cmd>GitConflictChooseTheirs<cr>"  "Adopt incoming changes"]
    :n ["<cmd>GitConflictNextConflict<cr>"  "Next conflict"]
    :o ["<cmd>GitConflictChooseOurs<cr>"    "Adopt our changes"]
    :p ["<cmd>GitConflictPrevConflict<cr>"  "Previous conflict"]
    :q ["<cmd>GitConflictListQf<cr>"        "Open quickfix"]
    :r ["<cmd>GitConflictChooseNone<cr>"    "Reject both changes"]}}
