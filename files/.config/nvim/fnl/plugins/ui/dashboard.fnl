(import-macros {: augroup!
                : autocmd!} :fnl/macros/autocmd)


(local dashboard (require :dashboard))
(local splashes (.. (vim.fn.stdpath :config) :splashes))


(fn render-header []
  (let [header ["                                                  "
                "███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗"
                "████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║"
                "██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║"
                "██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║"
                "██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║"
                "╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝"
                "                                                  "]
        splash (.. "[ " ((-> (vim.fn.system (.. "shuf -n 1 " splashes)) (: :sub)) 1 -2) "! ]")]
    ;; Add the splash string followed by two empty lines
    (table.insert header splash)
    (table.insert header (. header 1))
    (table.insert header (. header 1))
    (table.insert header (. header 1))
    header))


(local newfile {:icon "  "
                :action :enew})

(local findfile {:icon "  "
                 :action "Telescope find-files hidden=true no-ignore=true"})

(local dots {:icon "  "
             :action (.. "Telescope find-files cwd=~/.config/nvim/"
                         " search-dirs=Ultisnips,lua,init.lua,splashes")})

(local update {:icon "  "
               :action :PackerSync})

(local term {:icon "  "
             :action :FloatermToggle})

(local close {:icon "  "
              :action :qa!})

(tset newfile  :desc "New file                             ")
(tset findfile :desc "Find file                            ")
(tset dots     :desc "Browse dotfiles                      ")
(tset update   :desc "Update plugins                       ")
(tset term     :desc "Open floating terminal               ")
(tset close    :desc "Close neovim                         ")

(tset newfile  :shortcut "      :enew")
(tset findfile :shortcut "      SPC f")
(tset dots     :shortcut "    SPC v d")
(tset update   :shortcut ":PackerSync")
(tset term     :shortcut "    SPC t t")
(tset close    :shortcut "       :qa!")


(tset dashboard :custom-header render-header)
(tset dashboard :custom-center [newfile
                                findfile
                                dots
                                update
                                term
                                close])

(tset dashboard :custom-footer [""
                                (.. "neovim loaded "
                                    (length (vim.tbl_keys :packer-plugins))
                                    " plugins")])

(let [au (augroup! :dashboard)]
  (autocmd! au :FileType
            {:desc "Quit dashboard with `q`"
             :pattern :dashboard
             :callback (fn []
                         (vim.api.nvim-buf-set-keymap 0 :n :q :<cmd>quit!<cr>
                                                      {:noremap true
                                                       :silent true}))}))
