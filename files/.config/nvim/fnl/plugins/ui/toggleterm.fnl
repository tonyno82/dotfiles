(local floor vim.fn.floor)


((. (require :toggleterm) :setup)
  {:size (fn [term]
           (if (= term.direction :horizontal)
                 (* vim.o.lines 0.4)
               (= term.direction :vertical)
                 (* vim.o.columns 0.4)))
   :open_mapping :<c-\>
   :shading_factor -30
   :float_opts {:border :curved
                :shading_factor -30
                :height (fn [] (floor (* vim.o.lines 0.5)))
                :width  (fn [] (floor (* vim.o.columns 0.5)))}})
