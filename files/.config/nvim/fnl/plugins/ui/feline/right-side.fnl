(local lsp (require :plugins.ui.feline.lsp))
(local separator (require :plugins.ui.feline.separator))
(local vi-mode (require :plugins.ui.feline.vi-mode))

(local api vim.api)
(local colors ((. (require :tokyonight.colors) :setup)))
(local devicons (require :nvim-web-devicons))


(fn cursor-info []
  (let [cursor (api.nvim_win_get_cursor 0)
        lines  (api.nvim_buf_line_count 0)]
    (.. (. cursor 1) "·" lines ":" (. cursor 2))))


(fn display-diagnostics [severity symbol]
  (let [diagnostics (vim.diagnostic.get 0
                      {:severity (. vim.diagnostic.severity severity)})
        count (length diagnostics)]
    (-> (> count 0) (and (.. " " symbol count " ")) (or ""))))


(fn file-info []
  (let [format-icon {:dos " "
                     :mac " "
                     :unix " "}
        encoding (string.upper vim.o.fileencoding)
        format   (. format-icon vim.o.fileformat)]
    (.. encoding (if (not= encoding "") " " "") format "")))


[{:provider (fn [] (.. separator.thin-left " " vim.o.filetype " "))
  :enabled (fn [] (not= vim.o.filetype ""))
  :hl {:bg colors.bg_highlight
       :fg colors.fg
       :style :bold}}

 {:provider (fn []
              (let [filename (vim.api.nvim_buf_get_name 0)
                    icon (devicons.get_icon filename)]
                (-> icon (and (.. icon " ")) (or ""))))
  :enabled (fn [] (not= vim.o.filetype ""))
  :hl (fn []
        (let [filename (vim.api.nvim_buf_get_name 0)
              (_ color) (devicons.get_icon_color filename)]
          {:bg colors.bg_highlight
           :fg color
           :style :bold}))}

 {:provider separator.left
  :enabled (fn [] (lsp.update) (lsp.clients-are-attached))
  :hl {:bg colors.bg_highlight
       :fg colors.magenta2}}

 {:provider (fn [] (display-diagnostics :ERROR " "))
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.magenta2
       :fg colors.bg_highlight
       :style :bold}}

 {:provider separator.left
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.magenta2
       :fg colors.orange}}

 {:provider (fn [] (display-diagnostics :WARN " "))
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.orange
       :fg colors.bg_highlight
       :style :bold}}

 {:provider separator.left
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.orange
       :fg colors.green}}

 {:provider (fn [] (display-diagnostics :HINT " "))
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.green
       :fg colors.bg_highlight
       :style :bold}}

 {:provider separator.left
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.green
       :fg colors.blue0}}

 {:provider (fn [] (display-diagnostics :INFO " "))
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.blue0
       :fg colors.fg
       :style :bold}}

 {:provider separator.left
  :hl (fn []
        (let [bg (-> (lsp.clients-are-attached) (and colors.blue0) (or colors.bg_highlight))]
          {:bg bg
           :fg colors.green2}))}

 {:provider (fn [] (.. "  " (lsp.clients-names) " "))
  :enabled (fn [] (lsp.clients-are-attached))
  :hl {:bg colors.green2
       :fg colors.bg_highlight
       :style :bold}}

 {:provider separator.left
  :hl {:bg colors.green2
       :fg colors.terminal_black}}

 {:provider (fn [] (.. " " (file-info) " "))
  :hl {:bg colors.terminal_black
       :fg colors.fg
       :style :bold}}

 {:provider separator.left
  :hl (fn [] {:bg colors.terminal_black
              :fg (. (vi-mode.current_hl) :bg)})}

 {:provider (fn [] (.. "  " (cursor-info) " "))
  :hl (fn []
        (let [hl (vi-mode.current_hl)]
          {:bg hl.bg
           :fg hl.fg
           :style :bold}))}]
