(local feline (require :feline))
(local colors ((. (require :tokyonight.colors) :setup)))

(local left-side  (require :plugins.ui.feline.left-side))
(local right-side (require :plugins.ui.feline.right-side))
;; (local winbar     (require :plugins.ui.feline.winbar))


(local line_components {:active [left-side right-side]
                        :inactive []})

;; (local winbar_components {:active [winbar]
;;                           :inactive []})

(local force_inactive {:filetypes []
                       :buftypes  []
                       :bufnames  []})


(feline.setup {:components line_components
               :force_inactive force_inactive
               :theme colors})


;; (local belkan (require :belkan))

;; (belkan.start (fn [game-line]
;;     (tset (. left-side 13) :provider (.. " " game-line))))

;; (feline.winbar.setup {:components winbar_components
;;                       :force_inactive force_inactive
;;                       :theme colors})
