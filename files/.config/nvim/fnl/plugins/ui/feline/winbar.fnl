(local colors (. (require :tokyonight.colors) :setup))
(local gps    (require :nvim-gps))
(local navic (require :nvim-navic))

;; (gps.setup {:separator "  "})

(navic.setup {:highlight true
              :separator "  "
              :depth-limit 0
              :depth-limit-indicator "..."})


;; {{:provider (fn [] (gps.get-location))
;;   :enabled  (fn [] (gps.is-available))
;;   :hl {:bg colors.bg-dark
;;        :fg colors.fg}}}

[{:provider (fn [] (navic.get-location))
  :enabled  (fn [] (navic.is-available))}]
