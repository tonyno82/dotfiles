(local git (require :plugins.ui.feline.git))
(local vi_mode (require :plugins.ui.feline.vi-mode))
(local separator (require :plugins.ui.feline.separator))

(local colors ((. (require :tokyonight.colors) :setup)))
(local dap (require :dap))


[{:provider (fn [] (.. " " (vi_mode.current_mode) " "))
  :update [:ModeChanged]
  :hl (fn []
        (let [hl (vi_mode.current_hl)]
          {:bg hl.bg
           :fg hl.fg
           :style :bold}))}

 {:provider separator.right
  :hl (fn []
        (let [hl (vi_mode.current_hl)]
          {:bg colors.fg_gutter
           :fg hl.bg}))}

 {:provider (fn [] (.. "  " git.status.head " "))
  :enabled (fn [] (git.update) (git.file-is-in-repo))
  :hl {:bg colors.fg_gutter
       :fg colors.fg
       :style :bold}}

 {:provider separator.right
  :hl (fn []
    {:bg (-> (git.file-is-in-repo) (and colors.green) (or colors.bg_highlight))
     :fg colors.fg_gutter})}

 {:provider (fn []
              (let [added (or git.status.added 0)]
                (-> (> added 0) (and (.. "  " added " ")) (or ""))))
  :enabled (fn [] (git.file-is-in-repo))
  :hl {:bg colors.green
         :fg colors.bg_highlight
         :style :bold}}

 {:provider separator.right
  :enabled (fn [] (git.file-is-in-repo))
  :hl {:bg colors.orange
       :fg colors.green}}

 {:provider (fn []
              (let [changed (or git.status.changed 0)]
                (-> (> changed 0) (and (.. " 🡽 " changed " ")) (or ""))))
  :enabled (fn [] (git.file-is-in-repo))
  :hl {:bg colors.orange
       :fg colors.bg_highlight
       :style :bold}}

 {:provider separator.right
  :enabled (fn [] (git.file-is-in-repo))
  :hl {:bg colors.magenta2
       :fg colors.orange}}

 {:provider (fn []
              (let [removed (or git.status.removed 0)]
                (-> (> removed 0) (and (.. " 🞮 " removed " ")) (or ""))))
  :enabled (fn [] (git.file-is-in-repo))
  :hl {:bg colors.magenta2
       :fg colors.bg_highlight
       :style :bold}}

 {:provider separator.right
  :enabled (fn [] (git.file-is-in-repo))
  :hl {:bg colors.bg_highlight
       :fg colors.magenta2}}

 {:provider " DAP: "
  :enabled (fn [] (not= (dap.session) nil))
  :hl {:bg colors.bg_highlight
       :fg colors.fg
       :style :bold}}

 {:provider (fn [] (dap.status))
  :enabled (fn [] (not= (dap.session) nil))
  :hl {:bg colors.bg_highlight
       :fg colors.fg}}

 {:provider ""
  :hl {:bg colors.bg_highlight
       :fg colors.fg}}]
