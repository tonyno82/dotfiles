(local separator (require :plugins.ui.feline.separator))
(local colors (. (require :tokyonight.colors) :setup))


(var time "")
(var hl [])

(fn update-time []
  ;; local time_str = vim.fn.system([[date "+%H:%M"]]):sub(1, -2)
  ;; local hour = tonumber(time_str:sub(1, 2))
  (let [time-str (-> (vim.fn.system "date \"+%H:%M\"") (: :sub 1 -2))
        hour (-> time-str (: :sub 1 2) (tonumber))]
    (if (> hour 20) (do
          (set time (.. "  " time-str))
          (set hl {:bg colors.blue7
                   :fg colors.fg}))
        (> hour 17) (do
          (set time (.. "  " time-str))
          (set hl {:bg colors.green
                   :fg colors.bg}))
        (> hour 11) (do
          (set time (.. "  " time-str))
          (set hl {:bg colors.blue1
                   :fg colors.bg}))
        (> hour 7) (do
          (set time (.. "  " time-str))
          (set hl {:bg colors.yellow
                   :fg colors.bg}))
        (do
          (set time (.. "  " time-str))
          (set hl {:bg colors.blue7
                   :fg colors.fg})))))


[{:provider (fn [] (update-time) separator.left)
  :hl (fn []
        {:bg colors.bg
          :fg hl.bg})}

  {:provider (fn [] (.. " " time " "))
  :hl (fn []
        {:bg hl.bg
          :fg hl.fg})}]
