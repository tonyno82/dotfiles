(local LSP {:clients []})


(fn LSP.clients-are-attached []
  (not= (next LSP.clients) nil))


(fn LSP.clients-names []
  (let [clients []]
    (when LSP.clients
      (each [_ client (pairs LSP.clients)]
        (tset clients (+ (length clients) 1) client.name)))
      (table.concat clients " ")))


(fn LSP.update []
  (set LSP.clients (vim.lsp.get_active_clients {:bufnr 0})))


LSP
