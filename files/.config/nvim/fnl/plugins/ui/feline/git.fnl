(local Git {:status []})


(fn Git.file-is-in-repo []
  (not= Git.status nil))


(fn Git.update []
  (set Git.status vim.b.gitsigns_status_dict))


Git
