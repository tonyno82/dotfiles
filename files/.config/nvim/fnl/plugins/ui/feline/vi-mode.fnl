(local vim-mode vim.fn.mode)
(local colors ((. (require :tokyonight.colors) :setup)))


(local mode-bgs {:R   colors.purple
                 :Rv  colors.purple
                 :S   colors.orange
                 :V   colors.red
                 "" colors.orange
                 "" colors.red
                 :!   colors.red
                 :r?  colors.cyan
                 :c   colors.magenta
                 :i   colors.green
                 :ic  colors.yellow
                 :ix  colors.yellow
                 :n   colors.cyan
                 :r   colors.cyan
                 :rm  colors.cyan
                 :s   colors.orange
                 :t   colors.purple
                 :v   colors.red})

(local mode-names {:R   :REPLACE
                   :Rv  :REPLACE
                   :S   :SELECT
                   :V   :VISUAL
                   "" :SELECT
                   "" :VISUAL
                   :!   :SHELL
                   :r?  :CONFIRM
                   :c   :COMMAND
                   :i   :INSERT
                   :ic  :INSERT
                   :ix  :INSERT
                   :n   :NORMAL
                   :r   :WAITING
                   :rm  :MORE...
                   :s   :SELECT
                   :t   :TERMINAL
                   :v   :VISUAL})


(local ViMode [])


(fn ViMode.current_hl []
  {:bg (. mode-bgs (vim-mode))
   :fg colors.bg})


(fn ViMode.current_mode []
   (. mode-names (vim-mode)))


ViMode
