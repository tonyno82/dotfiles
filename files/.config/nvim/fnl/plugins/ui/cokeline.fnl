(local colors ((. (require :tokyonight.colors) :setup)))
(local devicons ((. (require :nvim-web-devicons) :get_icons)))

(local bg-dark colors.bg_dark)
(local bg-highlight colors.bg_highlight)
(local fg colors.fg)
(local yellow colors.yellow)


(local buf-name {:fg (fn [buf] (-> buf.is_modified (and yellow) (or fg)))
                 :style (fn [buf] (-> buf.is_focused (and :bold) (or nil)))
                 :text (fn [buf] (.. buf.unique_prefix buf.filename))})


(local devicon
  {:text (fn [buf]
           (if (= buf.devicon.icon " ")
                 (let [new-devicon (. devicons buf.filetype)]
                   (if new-devicon (do
                         (set buf.devicon.color new-devicon.color)
                         (set buf.devicon.icon (.. new-devicon.icon " ")))
                       (set buf.devicon.icon " "))))
           buf.devicon.icon)
   :fg (fn [buf] buf.devicon.color)})


(local left-separator {:text (fn [buf] (-> (not= buf.index 1) (and " ") (or " ")))
                       :bf (fn [buf] (-> buf.is_focused (and bg-highlight) (or bg-dark)))
                       :fg bg-dark})

(local readonly-marker {:text (fn [buf] (-> buf.is_readonly (and " ") (or "")))
                        :fg yellow})

(local right-separator {:text "█"
                        :bg bg-dark
                        :fg (fn [buf] (-> buf.is_focused (and bg-highlight) (or bg-dark)))})


((. (require :cokeline) :setup)
 {:components [left-separator
               devicon
               buf-name
               readonly-marker
               right-separator]
  :default_hl {:bg (fn [buf] (-> buf.is_focused (and bg-highlight) (or bg-dark)))
               :fg colors.fg}})


(vim.cmd (.. "hi! TabLineFill guibg=" bg-dark))


(local keymap-set vim.keymap.set)
(local keymap-opts {:noremap true
                    :silent true})
(local maps {:<C-n>   "<Plug>(cokeline-focus-next)"
             :<C-p>   "<Plug>(cokeline-focus-prev)"
             :<C-A-n> "<Plug>(cokeline-switch-next)"
             :<C-A-p> "<Plug>(cokeline-switch-prev)"})

(each [keys action (pairs maps)]
  (keymap-set :n keys action keymap-opts))
