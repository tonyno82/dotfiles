(import-macros {: plug!} :fnl/macros/plugins)

(local vfn vim.fn)


(fn autoinstall-on-clean-session []
  (let [install-path (.. (vfn.stdpath :data) :/lazy/lazy.nvim)]
    (var autoinstalled false)
    (when (not (vim.loop.fs_stat install-path))
      (vfn.system [:git :clone "https://github.com/folke/lazy.nvim.git"
                   "--filter=blob:none"
                   install-path])
      (set autoinstalled true))
    (vim.opt.rtp:prepend install-path)
    autoinstalled))


(local autoninstalled (autoinstall-on-clean-session))

((. (require :lazy) :setup) [(require :plugins.core)
                             (require :plugins.editor)
                             (require :plugins.files)
                             (require :plugins.git)
                             (require :plugins.lsp)
                             (require :plugins.ui)])

  ;; (if _G.GROCTEL_PLUGDEV (do
  ;;   (use (plug! (.. (os.getenv :HOME) :/Documents/Git/Groctel/welcome-nvim)
  ;;               {:config (fn [] (require :plugins.ui.welcome))}))
  ;;   (use (plug! (.. (os.getenv :HOME) :/Documents/Git/Groctel/nvim-belkan)
  ;;               {:config (fn [] (require :plugins.ui.belkan))})))
  ;;   ;; (use (plug! (.. (os.getenv :HOME) :/Documents/Git/Groctel/nvim-formatter)
  ;;   ;;             {:lazy true
  ;;   ;;              :cmd :Format}))
  ;;   (use (plug! :glepnir/dashboard-nvim
  ;;               {:config (fn [] (require :plugins.ui.dashboard))})))
  ;; (when clean-session
  ;;   (packer.sync))))
