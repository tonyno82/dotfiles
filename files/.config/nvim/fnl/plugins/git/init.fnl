(import-macros {: plug!} :fnl/macros/plugins)


[(plug! :tpope/vim-fugitive)

 (plug! :akinsho/git-conflict.nvim
        {:config (fn [] (require :plugins.git.git-conflict))})

 (plug! :idanarye/vim-merginal
        {:lazy true
         :cmd :Merginal})

 (plug! :lewis6991/gitsigns.nvim
       {:config (fn [] (require :plugins.git.gitsigns))})

 (plug! :sindrets/diffview.nvim
        {:lazy true
         :cmd [:DiffviewOpen
               :DiffviewLog
               :DiffviewClose
               :DiffviewRefresh
               :DiffviewFocusFiles
               :DiffviewFileHistory
               :DiffviewToggleFiles]
         :config (fn [] (require :plugins.git.diffview))})

 (plug! :TimUntersberger/neogit
        {:lazy true
         :dependencies [:diffview.nvim]
         :cmd :Neogit
         :config (fn [] (require :plugins.git.neogit))})]
