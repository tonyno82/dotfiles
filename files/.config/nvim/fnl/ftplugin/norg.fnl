(local opt vim.opt_local)

(tset opt :expandtab true)
(tset opt :tabstop 2)
(tset opt :shiftwidth 2)
(tset opt :textwidth 80)
(tset opt :commentstring "%%s%")
(tset opt :foldlevel 10)
