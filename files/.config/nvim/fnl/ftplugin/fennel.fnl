(local opt vim.opt_local)

(tset opt :expandtab true)
(tset opt :tabstop 2)
(tset opt :shiftwidth 2)
