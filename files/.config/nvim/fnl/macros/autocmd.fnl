(fn augroup! [name]
  `(vim.api.nvim_create_augroup (.. :AU_ ,name) {:clear true}))

(fn autocmd! [group events opts]
  (tset opts :group group)
  `(vim.api.nvim_create_autocmd ,events ,opts))


{: augroup!
 : autocmd!}
