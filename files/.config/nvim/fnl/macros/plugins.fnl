(fn plug! [name args]
  (if
    args (do
      (tset args 1 name)
      args)
    name))

{: plug!}
