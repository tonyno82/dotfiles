set RANGER_LOAD_DEFAULT_RC=FALSE

PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"
PATH="$PATH:$HOME/.local/bin"
PATH="$PATH:$HOME/.yarn/bin"
EDITOR="vim"

# ANTIGEN SETTINGS

source /usr/share/zsh/share/antigen.zsh

antigen use oh-my-zsh
antigen bundle adb
antigen bundle alias-finder
antigen bundle battery
antigen bundle colored-man-pages
antigen bundle command-not-found
antigen bundle copybuffer
antigen bundle copyfile
antigen bundle git
antigen bundle ripgrep
antigen bundle rust
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen apply

alias cat="bat"
alias gid="git idiff"
alias gids="git idiff --staged"
alias l="exa -lah --group-directories-first -gs extension"
alias ll="exa -lh --group-directories-first -gs extension"
alias ls="exa"
alias norg="norg -g"
alias sudo="sudo "
alias t="tree -a"
alias tt="tree"

7zz ()
{
	dst="$1"
	src="$2"

	# if [ -z "$(echo "$dst" | grep "\.7z$")" ] && [ $# -eq 1 ]; then
	if ! echo "$dst" | grep "\.7z$" && [ $# -eq 1 ]; then
		src="$dst"
		dst="$dst.7z"
	fi

	7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on "$dst" "$src"
}


yaygit ()
{
	EDITOR="vim" yay -Syu --editmenu --needed $(yay -Qeq | grep ".*-git" | tr '\n' ' ')
}


venv ()
{
	root="$HOME/.venv"
	env="$1"

	[ -d "$root" ] || mkdir -p "$root"
	[ -d "$root/$env" ] || python -m virtualenv "$root/$env"
	source "$root/$env/bin/activate"
}


ytdl ()
{
	setopt shwordsplit
	YTDL_BASE="yt-dlp -N 8 --split-chapters"
	YTDL="$YTDL_BASE"

	while [ $# -gt 0 ]; do
		if [ "$1" = "m" ]; then
			YTDL="$YTDL_BASE -x --audio-quality 0"
		elif [ "$1" = "v" ]; then
			YTDL="$YTDL_BASE"
		elif echo "$1" | grep "playlist" > /dev/null; then
			$YTDL -o "%(playlist_title)s/%(playlist_index)s - %(title)s.%(ext)s" "$1"
		else
			$YTDL -o "%(title)s.%(ext)s" "$1"
		fi

		shift
	done
}


eval $(starship init zsh)

alias luamake="$HOME/Documents/Git/Fork/lua-language-server/3rd/luamake/luamake"
