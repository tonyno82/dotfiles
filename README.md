# Groctel's dotfiles

**Here are my dotfiles.
They are files made out of text on a directory tree with nothing.
Add them to your system if you want, I couldn't care less.**
*Ron Swanson*

## Installing the files

Use [`stow`](https://archlinux.org/packages/community/any/stow/) to create symlinks in your system:

```bash
stow -t "$HOME" files
```

## Legacy versions

- **Latest i3wm commit:** [`5ec02ca5806d57a442b87d681962112f8b255881`](5ec02ca5806d57a442b87d681962112f8b255881)
- **Latest `dotfiler.sh` commit:** [`dbf8775e2c13302e0afc2eb035303b7a2f3de701`](dbf8775e2c13302e0afc2eb035303b7a2f3de701)
